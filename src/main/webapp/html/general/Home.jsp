	<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" href="../${pageContext.request.contextPath}/css/styleindex.css" />

    <title>Index</title>
  </head>

  <body>
    <!-- ***********************  HEADER / NAVIGATION *********************** -->
    <header class="header">
      <div class="container logo-nav-container">
        <a href="#" class="logo">LOGO</a>
        <!-- This span is in order to deploy the menu nav bar when the screen is small -->
        <span class="menu-icon">Menu</span>
        <nav class="">
          <ul class="navigation">
            <li style="color: blue;"><a href="Home.html" data-section="Home">Home</a></li>
            <li><a href="#">Login</a>
              <ul class="sub-nav"> 
                <li><a href="loginadmin.html" data-section="Form">Admin</a></li>
                <li><a href="loginuser.html">User</a></li>
              </ul>  
            <li><a href="#">Signup</a>
            <li><a href="#">Contact</a></li>
           
          </ul>
        </nav>
      </div>
    </header>
    <!-- ***********************  BODY *********************** -->
    <main class="main">
      <section id="firstContent">
        <h1>Welcome to the library managment system</h1>
       
      </section>
        
        
    </main>
    <!-- ***********************  FOOTER *********************** -->
    <footer class="footer">
      <div class="contact-info">
        <p>
          <strong>Contact info</strong>
          <br />
          Place
          <br />
          Queretaro
          <br />
          Other places
          <br />
          || CDMX || Guadalajara
          <br />
          Phone
          <br />
          2461103631
        </p>
      </div>
      <div class="copyright-container">
        <ul class="copyright-list">
          <li>© 2020 Test page for learning</li>
          <li><a href="Home.html" data-section="Home">|| Index</a></li>
          <li>
            <a href="#" data-section="Form">|| Sign up</a>
          </li>
        </ul>
      </div>
    </footer>
  </body>
</html>
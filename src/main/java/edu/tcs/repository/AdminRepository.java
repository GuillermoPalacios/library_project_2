package edu.tcs.repository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import java.util.Properties;
import java.util.Random;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


import edu.tcs.model.Admin;


import edu.tcs.utilities.JPA_Util;

public class AdminRepository {
	
	EntityManager entity = JPA_Util.getEntityManagerFactory().createEntityManager();
	
	
	//Working properly
	public void addAdmin(Admin admin) {
		
        EntityTransaction transaction = entity.getTransaction();

        transaction.begin();
        //Here the date is in the format that I want to use
          
        entity.persist(admin);
 
        //For createNativeQuery i have to used the name of my table
        transaction.commit();
          entity.close();
          
	}
	
	//Working properly
	public boolean loginAdmin(String user, String password)
		  {
		       try{
		    	   EntityTransaction transaction = entity.getTransaction();
		    	   transaction.begin();

//		    	TypedQuery<Users> query = em.createQuery("SELECT u FROM users u WHERE u.login = :login AND u.password = :pass", Users.class);        
		    	TypedQuery<Admin> query = entity.createQuery("SELECT a FROM Admin a WHERE a.admin_name = :user AND a.admin_password = :password", Admin.class);
		        query.setParameter("user", user);
		        query.setParameter("password", password); 
		        try{ 
		            Admin a = query.getSingleResult();
		            return true;
		        }catch(javax.persistence.NoResultException e)
		        {
		            return false;
		        }
		        }
		        finally{
		        entity.close();
		        }

		    }
	public Admin getAdmin(Long id) {
		Admin admin = new Admin();
		admin= entity.find(Admin.class, id);
		return admin;
	}
	
	public void updateAdmin(Admin admin) {

		entity.getTransaction().begin();
		entity.merge(admin);
		entity.getTransaction().commit();
		entity.close();
	}
	//Working properly
	public void deleteAdmin(Long id) {
		Admin admin = new Admin();
		admin = entity.find(Admin.class, id);
		entity.getTransaction().begin();
		entity.remove(admin);
		entity.getTransaction().commit();
		
	}
	
	public void updateCode(Long id, String code) {
		entity.getTransaction().begin();
		Admin admin = new Admin();
    	admin= entity.find(Admin.class, id);
    	admin.setCode(code);
    	entity.merge(admin);
    	entity.getTransaction().commit();
		
	}
	
	public List<Admin> getAllBooks() {

		List<Admin> book_list = new ArrayList<Admin>();
		
		Query query = entity.createQuery("FROM Admin");
		book_list=query.getResultList();
		
		return book_list;
		

	}
	  public String getRandom() {
	        Random rnd = new Random();
	        int number = rnd.nextInt(999999);
	        return String.format("%06d", number);
	    }
    //Send email to the user email
	    public boolean sendEmail(Admin admin) {
	        boolean test = false;

	        String toEmail = admin.getEmail();
	        String fromEmail = "guipalaga@gmail.com";
	        String password = "asdasdas";

	        try {
	      
	            // your host email smtp server details
	            Properties pr = new Properties();
	            //it was not working because I didn't have gmail.com
	            //pr.setProperty("mail.smtp.host", "smtp.gmail.com");
	            
	            pr.setProperty("mail.smtp.host", "smtp.gmail.com");
	            pr.setProperty("mail.smtp.port", "587");
	            pr.setProperty("mail.smtp.auth", "true");
	            pr.setProperty("mail.smtp.starttls.enable", "true");
	            pr.put("mail.smtp.socketFactory.port", "587");
	            pr.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
	 
	            //get session to authenticate the host email address and password
	            Session session = Session.getInstance(pr, new Authenticator() {
	                @Override
	                protected PasswordAuthentication getPasswordAuthentication() {
	                    return new PasswordAuthentication(fromEmail, password);
	                }
	            });

	            //set email message details
	            Message mess = new MimeMessage(session);

	    		//set from email address
	            mess.setFrom(new InternetAddress(fromEmail));
	    		//set to email address or destination email address
	            mess.setRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));
	    		
	    		//set email subject
	            mess.setSubject("User Email Verification");
	            
	    		//set message text
	            mess.setText("Registered successfully.Please verify your account using this code: " + admin.getCode());
	            //send the message
	            Transport.send(mess);
	            
	            test=true;
	            
	        } catch (Exception e) {
	            e.printStackTrace();
	        }

	        return test;
	    }
	
}

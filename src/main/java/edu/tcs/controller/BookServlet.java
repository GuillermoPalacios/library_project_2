package edu.tcs.controller;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.tcs.model.Book;
import edu.tcs.model.IssueBook;

import edu.tcs.repository.BookRepository;
import edu.tcs.repository.IssueBookRepository;


@WebServlet(name="bookServlet", urlPatterns={
		"/Book_new_form", 
		"/Book_register",
		"/Book_delete",
		"/Book_edit",
		"/Book_update",
		"/Book_list",
		"/Book_issueBook"})
public class BookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private BookRepository bookRepository;
	private IssueBookRepository issueBookRepository;
	public void init() {
		bookRepository = new BookRepository();
		issueBookRepository= new IssueBookRepository();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.getWriter().append("Served at: ").append(request.getContextPath());

		String action = request.getServletPath();
		try {
			switch (action) {
			case "/Book_new_form":
				showNewForm(request, response);
				break;
			case "/Book_register":
				insertBook(request, response);
				break;
			case "/Book_delete":
				deleteBook(request, response);
				break;
			case "/Book_edit":
				showEditForm(request, response);
				break;
			case "/Book_update":
				updateBook(request, response);
				break;
			case "/Book_list":
				listBook(request, response);
				break;
		   	case "/Book_issueBook":
				issueBook(request, response);
				break;
			default:
				response.sendRedirect("Student.html");
				break;
			}
		} catch (SQLException | ParseException ex) {
			throw new ServletException(ex);
		}

	}
	
	
	private void issueBook(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException, ParseException {
		Long library_code = Long.parseLong(request.getParameter("library_code"));
		Long student_code = Long.parseLong(request.getParameter("student_code"));
		String issue_Date = request.getParameter("issue_date");
		String return_Date = request.getParameter("return_date");
	
		//Setting string into date format

		//Creating the object
		IssueBook issueBook = new IssueBook();
		issueBook.setLibrary_code(library_code);
		issueBook.setStudent_id(student_code);
		issueBook.setIssue_date(issue_Date);
		issueBook.setReturn_date(return_Date);
		
		
		issueBookRepository.issueABook(issueBook);
		

			RequestDispatcher dispatcher = request.getRequestDispatcher("/SuccessIssue.html");
			dispatcher.forward(request, response);
	
	}



	private void listBook(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {
		List<Book> listBooks = bookRepository.getAllBooks();
		request.setAttribute("listBooks", listBooks);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/BookList.html");
		dispatcher.forward(request, response);
	}

	private void showNewForm(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getRequestDispatcher("/RegisterForm.html");
		dispatcher.forward(request, response);
	}

	private void showEditForm(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, ServletException, IOException {
		Long id = Long.parseLong(request.getParameter("id"));
		Book existingBook = bookRepository.getBook(id);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/RegisterForm.html");
		request.setAttribute("book", existingBook);
		dispatcher.forward(request, response);

	}

	private void insertBook(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		// Adding a new book
		// Getting parameters from the jsp form
		String book_name = request.getParameter("bookName");
		String author = request.getParameter("author");
		String category = request.getParameter("category");
		int iSBN = Integer.parseInt(request.getParameter("ISBN"));
		int quantity = Integer.parseInt(request.getParameter("quantity"));
//		//Setting parameters in order to make some actions
		Book book = new Book();
		book.setBook_name(book_name);
		book.setAuthor(author);
		book.setCategory(category);
		book.setISBN(iSBN);
		book.setQuantity(quantity);

		bookRepository.addBook(book);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/Success.html");
		dispatcher.forward(request, response);

	}

	private void updateBook(HttpServletRequest request, HttpServletResponse response) throws SQLException, IOException {
		Long id = Long.parseLong(request.getParameter("id"));
		String book_name = request.getParameter("bookName");
		String author = request.getParameter("author");
		String category = request.getParameter("category");
		int iSBN = Integer.parseInt(request.getParameter("ISBN"));
		int quantity = Integer.parseInt(request.getParameter("quantity"));

		Book book = new Book(book_name, author, iSBN, category, quantity);
		bookRepository.updateBook(book);
		response.sendRedirect("/Book_list");
	}

	private void deleteBook(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {
		Long id = Long.parseLong(request.getParameter("id"));
		bookRepository.deleteBook(id);
		RequestDispatcher dispatcher = request.getRequestDispatcher("/Success.html");
		dispatcher.forward(request, response);
	}

}
package edu.tcs.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;

import edu.tcs.model.Admin;

import edu.tcs.repository.AdminRepository;

import edu.tcs.repository.StudentRepository;

@WebServlet(name="LoginServlet", urlPatterns={
											"/Login_passwordSendEmail", 
											"/Login_passwordReset",
											"/Login_newPassword",
											"/Login_loginadmin",
											"/Login_loginuser"})
//I will try use spring for this project
@Controller
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	private AdminRepository adminRepository;
	private StudentRepository studentRepository;

	public void init() {

		adminRepository = new AdminRepository();
		studentRepository= new StudentRepository();

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.getWriter().append("Served at: ").append(request.getContextPath());

		String action = request.getServletPath();
		try {
			switch (action) {
			case "/Login_passwordSendEmail":
				sendEmail(request, response);
				break;
			case "/Login_passwordReset":
				reviewCode(request, response);
				break;
			case "/Login_newPassword":
				resetPassword(request, response);
				break;
			case "/Login_loginadmin":
				loginAdmin(request, response);
				break;
			case "/Login_loginuser":
				loginUser(request, response);
				break;
			default:
				response.sendRedirect("index.html");
				break;
			}
		} catch (SQLException  ex) {
			throw new ServletException(ex);
		}

	}
	
			
	private void loginAdmin(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {
		String user_admin = request.getParameter("user");
		String password = request.getParameter("password");
		
		boolean test = adminRepository.loginAdmin(user_admin, password);
		if (test) {
	
			RequestDispatcher dispatcher = request.getRequestDispatcher("/admin.html");
			dispatcher.forward(request, response);
		} else {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/loginadmin.html");
			dispatcher.forward(request, response);
		}
	}
	
	private void loginUser(HttpServletRequest request, HttpServletResponse response)
				throws SQLException, IOException, ServletException {
			String student_user = request.getParameter("user");
			String password = request.getParameter("password");
			
			boolean test = studentRepository.loginStudent(student_user, password);
			if (test) {
				System.out.println("Login");
				RequestDispatcher dispatcher = request.getRequestDispatcher("/Student.html");
				dispatcher.forward(request, response);
			} else {
				RequestDispatcher dispatcher = request.getRequestDispatcher("/loginuser.html");
				dispatcher.forward(request, response);
			}
			
	}
	private void resetPassword(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {

		RequestDispatcher dispatcher = request.getRequestDispatcher("/Success.html");
		dispatcher.forward(request, response);
	}

	private void reviewCode(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {

		HttpSession session = request.getSession();
		Admin admin = (Admin) session.getAttribute("authcode");

		String code = request.getParameter("authcode");

		if (code.equals(admin.getCode())) {
			RequestDispatcher dispatcher = request.getRequestDispatcher("/passwordreset.html");
			dispatcher.forward(request, response);
		} else {
			System.out.println("Error");
		}
	}

	private void sendEmail(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException {
		response.setContentType("text/html;charset=UTF-8");
		try (PrintWriter out = response.getWriter()) {
			// fetch form value
			Long id = Long.parseLong(request.getParameter("user_id"));

			String code = adminRepository.getRandom();
			// Create new admin using the id
			Admin admin = adminRepository.getAdmin(id);
			// I am setting the code that is going to be send
			adminRepository.updateCode(admin.getAdmin_id(), code);

			// call the send email method
			boolean test = adminRepository.sendEmail(admin);

			// check if the email send successfully
			if (test) {
				HttpSession session = request.getSession();
				session.setAttribute("authcode", admin);
				RequestDispatcher dispatcher = request.getRequestDispatcher("/email.html");
				dispatcher.forward(request, response);
			} else {
				out.println("Failed to send verification email");
			}

		}

	}


}
package edu.tcs.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.tcs.model.IssueBook;
import edu.tcs.repository.IssueBookRepository;

@WebServlet(name="studentServlet", urlPatterns={
		"/Student_issueBook"}) 



public class StudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	private IssueBookRepository issueBookRepository;
	public void init() {

		issueBookRepository= new IssueBookRepository();
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.getWriter().append("Served at: ").append(request.getContextPath());

		String action = request.getServletPath();
		try {
			switch (action) {
			case "/Student_issueBook":
				issueBook(request, response);
				break;
			default:
				response.sendRedirect("index.html");
				break;
			}
		} catch (SQLException | ParseException ex) {
			throw new ServletException(ex);
		}

	}
	
	
	private void issueBook(HttpServletRequest request, HttpServletResponse response)
			throws SQLException, IOException, ServletException, ParseException {
		Long library_code = Long.parseLong(request.getParameter("library_code"));
		Long student_code = Long.parseLong(request.getParameter("student_code"));
		String issue_Date = request.getParameter("issue_date");
		String return_Date = request.getParameter("return_date");
	
		//Setting string into date format

		//Creating the object
		IssueBook issueBook = new IssueBook();
		issueBook.setLibrary_code(library_code);
		issueBook.setStudent_id(student_code);
		issueBook.setIssue_date(issue_Date);
		issueBook.setReturn_date(return_Date);
		
		
		issueBookRepository.issueABook(issueBook);
		

			RequestDispatcher dispatcher = request.getRequestDispatcher("/SuccessIssue.html");
			dispatcher.forward(request, response);
	
	}
	
}
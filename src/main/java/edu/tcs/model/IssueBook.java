package edu.tcs.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity(name="IssueBook")
@Table(name="IssueBook")
public class IssueBook implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "library_code")
	private Long   library_code;
	
	@Column(name = "student_id", length = 50)
	private Long student_id;
	
	@Column(name = "issue_date", length = 50)
	private String issue_date;

	@Column(name = "return_date", length = 50)
	private String return_date;
	
	
	public IssueBook() {
		super();
	}
	
	
	public IssueBook(Long library_code, Long student_id,String issue_date, String return_date) {
		super();
		this.library_code = library_code;
		this.student_id = student_id;
		this.issue_date = issue_date;
		this.return_date = return_date;
	}


	public Long getLibrary_code() {
		return library_code;
	}
	public void setLibrary_code(Long library_code) {
		this.library_code = library_code;
	}
	public Long getStudent_id() {
		return student_id;
	}
	public void setStudent_id(Long student_id) {
		this.student_id = student_id;
	}


	public String getIssue_date() {
		return issue_date;
	}


	public void setIssue_date(String issue_date) {
		this.issue_date = issue_date;
	}


	public String getReturn_date() {
		return return_date;
	}


	public void setReturn_date(String return_date) {
		this.return_date = return_date;
	}

	

}

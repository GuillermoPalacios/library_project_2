package edu.tcs.utilities;


import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPA_Util {
	
	private static final String PERSISTENCE_UNIT_NAME = "edu.tcs.hibernate";
	//This is going to content the connection to the db
	private static EntityManagerFactory factory;
		
	
	public static EntityManagerFactory getEntityManagerFactory() {
		if (factory==null) {
			//This factory is saving all the attributes of my DB
			factory=Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME);
		}
		return factory;				
	}
	
	
	public static void shutdown() {
		if (factory!=null) {
			factory.close();
		}		
	
}
}